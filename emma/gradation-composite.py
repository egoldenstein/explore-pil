"""
gradation-composite.py
Emma Rose Goldenstein
CSC355 Open Source Development
16 October 2020
Week 1: Pillow Experiments

Credit: Code related to the parser is based on a portion of an
example program created by the collective class and
Leon Tabak on the morning of 10/16/20. Code relating to
creating and using the gradation mask was based off
of https://note.nkmk.me/en/python-numpy-generate-gradation-image/
and https://note.nkmk.me/en/python-pillow-composite/

Description:
This program parses an image from the command line using argparse,
creates a contoured version of a picture and then enhances the edges
using ImageFilter, creates and saves a gradation mask using numpy and
Image, and then creates a composite fading from the original picture
to the contoured version using the mask.
"""

from PIL import Image
from PIL import ImageFilter
import argparse
import numpy as np


def main():
    # print statement to indicate program is running
    print("Running gradation-composite.py")

    # creates an ArgumentParser object
    parser = argparse.ArgumentParser()
    # adds a string argument for the purpose of indicating
    # what image the client wants to alter
    parser.add_argument("file_name", type=str, help="image file")
    # parses the arguments from the command line
    args = parser.parse_args()

    # read an image from a file
    picture = Image.open(args.file_name)

    # produces a smaller version of the picture
    original_picture = picture.reduce(2)

    # shows original image for comparison
    original_picture.show()

    # creates the variables width and height with values
    # that correspond to the size of the given picture
    width, height = picture.size

    # contours the picture
    altered_picture = original_picture.filter(ImageFilter.CONTOUR)
    # enhances the edges to make the image crisper
    altered_picture = altered_picture.filter(ImageFilter.EDGE_ENHANCE)

    # shows altered picture for comparison
    altered_picture.show()

    """
    Defines a function that generates a 2D ndarray that increases or 
    decreases at equal intervals in the vertical or horizontal direction. This nd
    array corresponds to a monochrome gradation image. The value changes in the horizontal 
    direction when is_horizontal is True, and in the vertical direction when False. 
    For vertical orientation, use .T to create a transposed matrix.
    """
    def get_gradation_2d(start, stop, width, height, is_horizontal):
        if is_horizontal:
            return np.tile(np.linspace(start, stop, width), (height, 1))
        else:
            return np.tile(np.linspace(start, stop, height), (width, 1)).T

    """
    Expands to three dimensions. Sets start, stop, and is_horizontal for each color in a list, 
    and uses the function for 2D, to create a gradation image for each channel.
    """
    def get_gradation_3d(width, height, start_list, stop_list, is_horizontal_list):
        result = np.zeros((height, width, len(start_list)), dtype=np.float)

        for i, (start, stop, is_horizontal) in enumerate(zip(start_list, stop_list, is_horizontal_list)):
            result[:, :, i] = get_gradation_2d(start, stop, width, height, is_horizontal)

        return result

    # creates a mask array the size of the other images that will create a dark to light gradation horizontally
    mask_array = get_gradation_3d(width, height, (0, 0, 0), (255, 255, 255), (True, True, True))
    # creates and saves a mask image from the array
    Image.fromarray(np.uint8(mask_array)).save('images/gradation_h.jpg', quality=95)
    # reads the mask_image from file and converts it to grayscale
    mask_image = Image.open('images/gradation_h.jpg').convert('L').resize(original_picture.size)
    # creates a composite image that fades from the original picture to the contoured picture using
    # the gradation mask
    composite = Image.composite(altered_picture, original_picture, mask_image)

    # shows the composite image
    composite.show()


# end of main()

if __name__ == "__main__":
    main()
