"""
simple-emboss.py
Emma Rose Goldenstein
CSC355 Open Source Development
16 October 2020
Week 1: Pillow Library Experiments

Credit: Code related to the parser is
based on a portion of an example created by the
collective class and Leon Tabak on the
morning of 10/16/20

Description:
This program takes an image file as an
argument from the command line using argparse
then creates and shows an embossed version
of the original image half the size of the
original picture using ImageFilter.
"""

from PIL import Image
from PIL import ImageFilter
import argparse


def main():
    # print statement to indicate program is running
    print('Running simple-emboss.py')

    # creates an ArgumentParser object
    parser = argparse.ArgumentParser()
    # adds a string argument for the purpose of indicating
    # what image the client wants to alter
    parser.add_argument("file_name", type=str, help="image file")
    # parses the arguments from the command line
    args = parser.parse_args()

    # reads the given image from a file
    picture = Image.open(args.file_name)

    # produce a smaller version of the picture (half the size)
    original_picture = picture.reduce(2)

    # enhances the edges to prepare for a cleaner embossed image
    altered_picture = original_picture.filter(ImageFilter.EDGE_ENHANCE_MORE)
    # embosses the picture
    altered_picture = altered_picture.filter(ImageFilter.EMBOSS)

    # shows original picture for comparison
    original_picture.show()
    # shows the embossed version
    altered_picture.show()


# end of main()

if __name__ == "__main__":
    main()
