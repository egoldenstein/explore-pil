# explore-halves.py
# Leon Hannah Tabak
# CSC355 Open Source Development
# 12 October 2020
#
# This program is a template for experiments
# with PIL functions.
# You can show half of an image in its original
# form side by side with an altered half of the image.
# In the current version, the alteration is a conversion
# from color to black and white.


import numpy as np
from PIL import Image
from PIL import ImageDraw
from PIL import ImageOps


def main():
    # read an image from a file
    picture = Image.open("images/flowers-wall.jpg")

    # produce a smaller version of the picture
    original_picture = picture.reduce(2)

    print("original_picture mode = ", original_picture.mode)
    print("original_picture size = ", original_picture.size)

    # produce a black and white image from the color image
    # to allow composition with the color image, make this
    # new image's mode "RGB"
    altered_picture = ImageOps.grayscale(original_picture).convert("RGB")

    print("altered_picture mode = ", altered_picture.mode)
    print("altered_picture mode = ", altered_picture.size)

    # create a numpy array with as many rows and columns
    # as the color and black and white images and a depth of one
    width, height = original_picture.size

    # initialize array with zeros
    # specify the type of the elements as 8-bit unsigned integers
    mask_data = np.zeros((height, width), dtype=np.uint8)

    # assign 255 to the all elements in the right half
    # of the array

    mask_data[:, width // 2:] = 255

    mask = Image.fromarray(mask_data, mode="L")

    print("mask mode = ", mask.mode)
    print("mask size = ", mask.size)

    # put the left half of altered image
    # and the right half of the original image in a single image
    half_and_half = Image.composite(original_picture, altered_picture, mask)

    # draw a vertical line segment to mark the boundary
    # between the left and right halves of the image
    draw = ImageDraw.Draw(half_and_half)

    # specify the endpoints of the line segment
    start = (width // 2, 0)
    end = (width // 2, height)

    # specify the color of the line segment
    fill_color = (255, 0, 0)

    # draw line segment that is 4 pixels wide
    draw.line([start, end], fill=fill_color, width=6)

    # display the image
    half_and_half.show()


# end of main()

if __name__ == "__main__":
    main()
