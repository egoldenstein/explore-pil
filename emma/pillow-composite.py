"""
pillow-composite.py
Emma Rose Goldenstein
CSC355 Open Source Development
16 October 2020
Week 1: Pillow Experiments

Credit:
Based on Leon Tabak's explore-numpy example.
This code only differs by adding an argument parser
so that the program can be applied to any given image,
and creating a different mask.
Code related to the parser is based on a portion of an
example program created by the collective class and
Leon Tabak on the morning of 10/16/20

Description:
Program creates takes a picture, creates an all black image
the size the original picture and then creates a mask of the
same size with skinny black horizontal and vertical lines.
Then the program use Image.composite on the original image,
the all black image, and the mask to create a new image that
looks like the original photo with added thin black lines running
across the image vertically and horizontally.
"""

import numpy as np
from PIL import Image
import argparse


def main():
    # print statement to indicate program is running
    print("Running pillow-composite.py")

    # creates an ArgumentParser object
    parser = argparse.ArgumentParser()
    # adds a string argument for the purpose of indicating
    # what image the client wants to alter
    parser.add_argument("file_name", type=str, help="image file")
    # parses the arguments from the command line
    args = parser.parse_args()

    # reads the given image from a file
    picture = Image.open(args.file_name)

    # creates the variables width and height with values
    # that correspond to the size of puddle-trees.JPG
    width, height = picture.size

    # creates an all black image the size of puddle-trees.JPG
    black_image = Image.new('RGB', (width, height), color=0)

    """
     Creates an array with elements that correspond to the
     number of pixels in an image the size of the
     other images in order to edit and create an image mask 
     from the array down the line. Also, fills the array with
     255s with an unsigned 8 bit int data type, essentially 
     turning the image white.  
    """
    mask = np.full((height, width), 255, dtype=np.uint8)

    """
     FROM HERE ON OUT I WILL DESCRIBE CHANGES
     TO THE ARRAY AND ITS ELEMENTS AS IF IS ALREADY THE
     MASK IMAGE CREATED BY THE ARRAY LATER ON
    """

    # changes a pixel to black every 50 pixels horizontally
    # to create vertical black lines across the mask
    for i in range(0, width, 50):
        mask[:, i] = 0

    # changes a pixel to black every 50 pixels vertically
    # to add horizontal black lines to the mask
    for i in range(0, height, 50):
        mask[i, :] = 0

    # creates a mask image from the mask array
    mask_image = Image.fromarray(mask, 'L')

    # creates a composite image from the original picture,
    # black image and mask
    composite = Image.composite(picture, black_image, mask_image)

    # shows the composite image
    composite.show()


if __name__ == "__main__":
    main()
