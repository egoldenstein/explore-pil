"""
simple-kernel.py
Emma Rose Goldenstein
CSC355 Open Source Development
16 October 2020
Week 1: Pillow Library Experiments

Credit: Code related to the parser is
based on a portion of an example created by the
collective class and Leon Tabak on the
morning of 10/16/20. Other code based on
an example at https://tinyurl.com/y3t4tjqq
Kernel code based off of https://setosa.io/ev/image-kernels/,
https://tinyurl.com/y2wx7amv
and https://tinyurl.com/y3t4tjqq

Description:
This program creates an type of embossed
image using ImageFilter, but instead of
using ImageFilter.EMBOSS the code creates
a convolution kernel with the kernel
weights to create a different kind of embossed
image.
"""

from PIL import Image
from PIL import ImageFilter
import argparse


def main():
    # print statement to indicate program is running
    print('Running simple-kernel.py')

    # creates an ArgumentParser object
    parser = argparse.ArgumentParser()
    # adds a string argument for the purpose of indicating
    # what image the client wants to alter
    parser.add_argument("file_name", type=str, help="image file")
    # parses the arguments from the command line
    args = parser.parse_args()

    # reads the given image from a file
    picture = Image.open(args.file_name)

    # produce a smaller version of the picture (half the size)
    original_picture = picture.reduce(2)

    # creates a sequence containing the kernel weights to emboss)
    km = (
        -2, -1, 0,
        -1, 1, 1,
        0, 1, 2
    )
    # creates a convolution kernel to emboss images
    k = ImageFilter.Kernel(
        size=(3, 3),  # kernel size given as (width, height)
        kernel=km,    # applies the kernel weights of km
        scale=sum(km),  # default
        offset=0  # default
    )
    # embosses the image per the convolution kernel's specifications
    altered_picture = picture.filter(k)

    # shows original picture for comparison
    original_picture.show()
    # shows new embossed picture
    altered_picture.show()


if __name__ == "__main__":
    main()
