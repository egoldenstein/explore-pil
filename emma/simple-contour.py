"""
simple-contour.py
Emma Rose Goldenstein
CSC355 Open Source Development
16 October 2020
Week 1: Pillow Experiments

Credit: Code related to the parser is
based on a portion of an example created by the
collective class and Leon Tabak on the
morning of 10/16/20

Description:
This program parses a str argument of an
image file from the command line
and uses ImageFilter to create a contoured
version of the image with enhanced edges
to make the image crisper/more defined.
"""

from PIL import Image
from PIL import ImageFilter
import argparse


def main():
    # print statement to indicate program is running
    print('Running simple-contour.py')

    # creates an ArgumentParser object
    parser = argparse.ArgumentParser()
    # adds a string argument for the purpose of indicating
    # what image the client wants to alter
    parser.add_argument("file_name", type=str, help="image file")
    # parses the arguments from the command line
    args = parser.parse_args()

    # reads the given image from a file
    picture = Image.open(args.file_name)

    # produce a smaller version of the picture
    original_picture = picture.reduce(2)

    # show original image for comparison
    original_picture.show()

    # contour the picture
    altered_picture = original_picture.filter(ImageFilter.CONTOUR)
    # enhance the edges to make the image crisper
    altered_picture = altered_picture.filter(ImageFilter.EDGE_ENHANCE)

    # shows the new contoured image
    altered_picture.show()


# end of main()

if __name__ == "__main__":
    main()
